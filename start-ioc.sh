#!/bin/bash

P=/opt/bde/R3.15.5/iocs/ccs1ioc-master
cd $P || exit 1
bash bde-ioc-start.sh 1

echo IOC stopped..
echo
echo Press Enter to close..
read
