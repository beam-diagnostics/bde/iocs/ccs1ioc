< envPaths

errlogInit(20000)

dbLoadDatabase("$(TLCCSAPP)/dbd/iocApp.dbd")
iocApp_registerRecordDeviceDriver(pdbbase)

# Resource string: USB::VID::PID::SERIAL::RAW
#epicsEnvSet("RSCSTR", "USB::0x1313::0x8089::M00414547::RAW")
epicsEnvSet("RSCSTR", "USB::0x1313::0x8089::M00462436::RAW")
epicsEnvSet("PREFIX", "CCS1:")
epicsEnvSet("PORT",   "CCS1")
epicsEnvSet("QSIZE",  "20")
epicsEnvSet("XSIZE",  "3648")
epicsEnvSet("YSIZE",  "1")
epicsEnvSet("NCHANS", "2048")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(TOP)/db:$(AUTOSAVE)/db:$(ADCORE)/db:$(ADMISC)/db:$(ADTLCCS)/db:$(TLCCSAPP)/db")

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "10000000")
epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST", "NO")
epicsEnvSet("EPICS_CA_ADDR_LIST", "localhost")

# Create a Thorlabs CCSxxx driver
# tlCCSConfig(const char *portName, int maxBuffers, size_t maxMemory, 
#             const char *resourceName, int priority, int stackSize)
tlCCSConfig("$(PORT)", 0, 0, "$(RSCSTR)")
dbLoadRecords("tlccs.template",  "P=$(PREFIX),R=,PORT=$(PORT),ADDR=0,TIMEOUT=1,NELEMENTS=$(XSIZE)")

# Create standard arrays plugin for a trace
NDStdArraysConfigure("TRACE1", 5, 0, "$(PORT)", 0, 0)
dbLoadRecords("NDTrace.template", "P=$(PREFIX),R=Trace1:,PORT=TRACE1,ADDR=0,TIMEOUT=1,TYPE=Float64,FTVL=DOUBLE,NELEMENTS=4000,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0,ENABLED=1")

## Load all other plugins using commonPlugins.cmd
< commonPlugins.cmd

set_requestfile_path(".")
set_requestfile_path("$(TLCCSAPP)/req")
set_requestfile_path("$(ADTLCCS)/req")
set_requestfile_path("$(ADMISC)/req")
set_requestfile_path("$(ADCORE)/req")
set_requestfile_path("$(CALC)/req")
set_savefile_path("../autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")
save_restoreSet_status_prefix("$(PREFIX)")
dbLoadRecords("save_restoreStatus.db", "P=$(PREFIX)")

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

iocInit()

# save things every thirty seconds
create_monitor_set("auto_settings.req", 30, "P=$(PREFIX)")

dbpf $(PREFIX)TlWavelengthDataTarget "Factory"
dbpf $(PREFIX)TlWavelengthDataGet 1

dbpf $(PREFIX)TlAmplitudeDataMode "Current"
dbpf $(PREFIX)TlAmplitudeDataTarget "User"
dbpf $(PREFIX)TlAmplitudeDataGet 1
