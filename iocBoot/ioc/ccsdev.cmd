tlCCSConfig("$(PORT)", 0, 0, "$(RSCSTR)", 0, 100000)
dbLoadRecords("tlccs.template",  "P=$(PREFIX),R=,PORT=$(PORT),ADDR=0,TIMEOUT=1,NELEMENTS=$(XSIZE)")

# Create standard arrays plugin for a trace
NDStdArraysConfigure("$(PORT).TRACE1", 5, 0, "$(PORT)", 0, 0)
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=Trace1:,PORT=$(PORT).TRACE1,ADDR=0,TIMEOUT=1,TYPE=Float64,FTVL=DOUBLE,NELEMENTS=4000,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0,ENABLED=1")
