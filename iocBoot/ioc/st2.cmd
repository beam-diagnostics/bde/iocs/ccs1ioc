< envPaths

errlogInit(20000)

dbLoadDatabase("$(TOP)/dbd/tlccsAppDemo.dbd")
tlccsAppDemo_registerRecordDeviceDriver(pdbbase) 

# The search path for database files
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ADCORE)/db:$(ADTLCCS)/db")
epicsEnvSet("QSIZE",  "20")
epicsEnvSet("XSIZE",  "3648")
epicsEnvSet("YSIZE",  "1")
epicsEnvSet("NCHANS", "2048")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "10000000")

iocshLoad "ccsdev.cmd", "RSCSTR=USB::0x1313::0x8081::M00461031::RAW, PORT=CCS100, PREFIX=CCS100:"
iocshLoad "ccsdev.cmd", "RSCSTR=USB::0x1313::0x8089::M00462436::RAW, PORT=CCS200, PREFIX=CCS200:"

iocInit()

